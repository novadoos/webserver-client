import {
  Component,
  Input,
  OnInit
} from '@angular/core';

@Component({
  selector: 'search-box',
  styleUrls: ['./search-box.component.scss'],
  templateUrl: './search-box.component.html'
})

export class SearchBoxComponent implements OnInit {
  @Input() public name: string;
  public ngOnInit() {
    console.log('SearchBox component loaded');
  }
}
