import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { SearchService } from './services/search.service';
import { CardComponent } from './card';
import { VideoModel } from '../common/models/video.model';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'search',
  providers: [SearchService],
  styleUrls: ['./search.component.scss'],
  templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit {
  public innerHeight: any;
  public allVideos: VideoModel[];

  constructor(private service: SearchService) {
    this.innerHeight = (innerHeight - 50) + 'px';
  }

  public ngOnInit() {
    console.log('hello `Search` component');
    console.log(this.innerHeight);
  }

  public getVideos() {
    this.service.getVideos().subscribe((videos) => {
      this.allVideos = videos;
      console.log(this.allVideos.length);
    });
  }
}
