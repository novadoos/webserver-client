import {
  Component,
  Input,
  OnInit
} from '@angular/core';

@Component({
  selector: 'video-card',
  styleUrls: ['./card.component.scss'],
  templateUrl: './card.component.html'
})

export class CardComponent implements OnInit {
  @Input() public name: string;
  public ngOnInit() {
    console.log('Card component loaded');
  }
}
