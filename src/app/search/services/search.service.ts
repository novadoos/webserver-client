import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { VideoModel } from '../../common/models/video.model';

@Injectable()
export class SearchService {
  constructor(private http: Http) { }

  public getVideos(): Observable<VideoModel[]> {
    return this.http.get('https://localhost:3333/api/videos').map(this.extractData);
  }

  private extractData(res: Response) {
    return res.json();
  }
}
