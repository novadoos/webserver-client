export class VideoModel {
  id: string;
  name: string;
  description: string;
  users: any[]
}